# Вложенные классы и обобщения

## Вложенные классы

* Вложенный класс - это класс, определенный внутри другой области (класс, метод)

## Статический вложенный класс (просто вложенный)

* Объявляются с модификтором `static`
* Просто один код внутри другого
* Обрамляющий класс (внешний класс) имеет доступ ко всем членам вложенного класса
* Ассоциирован с внешним классом - следовательно, не имеет доступа к нестатическим членам класса

## Внутренний класс (inner)

* Объявляется без модификатора `static`
* Ассоциирован с объектом внешнего класса
* Имеет доступ ко всем членам внешнего класса

## Обобщения

### Использование и объявление дженериков

* Объявление - момент, когда мы объявляем тип, использующий обобщение - `class Type<T>`, `interface <T>`, `<T> type method(...)`
* Использование - момент, когда мы подставляем конкретный тип - `... extends Type<T>`, `Node<T> node`, `method(T a)`

### Bounds

* В экземпляр параметра generic-а типа T можно присвоить экземпляр любого потомка T