package ru.itis.nested.collections;

public interface Iterable {
    Iterator iterator();
}
