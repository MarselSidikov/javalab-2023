package ru.itis.nested.collections;

public class LinkedList implements Iterable {
    Node first;
    Node last;

    private int count;

    public void add(int value) {
        Node newNode = new Node(value);

        if (count == 0) {
            this.first = newNode;
        } else {
            this.last.next = newNode;
        }

        this.last = newNode;

        count++;
    }

    public int get(int index) {
        Node current = first;
        if (index >= 0 && index < count) {
            for (int i = 0; i < index; i++) {
                current = current.next;
            }

            return current.value;
        } else throw new IndexOutOfBoundsException();
    }

    @Override
    public Iterator iterator() {
        return new LinkedListIterator();
    }


    static class Node {
        final int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }
    }

    // inner class
    public class LinkedListIterator implements Iterator {

        private Node current;

        public LinkedListIterator() {
            this.current = first;
        }


        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public int next() {
            int value = current.value;
            this.current = current.next;
            return value;
        }
    }

}
