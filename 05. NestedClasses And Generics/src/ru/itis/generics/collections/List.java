package ru.itis.generics.collections;

public interface List<A> extends Iterable<A> {
    void add(A element);
}
