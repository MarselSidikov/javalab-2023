package ru.itis.services;

import ru.itis.dto.FileDto;

public interface FilesService {
    void upload(FileDto file);

    FileDto getFile(String fileName);
}
