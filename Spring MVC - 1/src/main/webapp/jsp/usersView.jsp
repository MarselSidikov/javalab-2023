<%@ page import="java.util.List" %>
<%@ page import="ru.itis.dto.UserDto" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Users from JSP</title>
</head>
<body>
<table>
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
    </tr>
    <%
        List<UserDto> users = (List<UserDto>) request.getAttribute("usersModel");
        for (UserDto user : users) {
    %>
    <tr>
        <td><%=user.getFirstName()%></td>
        <td><%=user.getLastName()%></td>
        <td><%=user.getEmail()%></td>
    </tr>
    <%}%>
</table>
</body>
</html>
