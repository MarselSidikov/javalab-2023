package main

import (
	"fmt"
	"sync"
)

var i = 0
var mutex sync.Mutex

func Print(wg *sync.WaitGroup) {
	mutex.Lock()
	fmt.Printf("До: %v \n", i)
	i++
	fmt.Printf("После: %v \n", i)
	mutex.Unlock()
	wg.Done()
}

func main() {

	var wg sync.WaitGroup

	wg.Add(3)

	go Print(&wg)
	go Print(&wg)
	go Print(&wg)

	wg.Wait()

	fmt.Println(i)
}
