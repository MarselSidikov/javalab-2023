package main

import (
	"fmt"
	"time"
)

var variable = 0

func PrintVariable(c chan int) {
	fmt.Printf("Начинаем чтение из канала ... \n")
	data := <-c // горутина блокируется, пока в канал ничего не положат
	fmt.Printf("Значение %v прочитано, увеличиваем \n", data)
	data++
	fmt.Printf("Записываем %v в канал ... \n", data)
	c <- data
	fmt.Printf("Значение %v записано \n", data)
}

func main() {

	var c = make(chan int) // канал, в котором может одно целое число int

	go PrintVariable(c)
	go PrintVariable(c)
	go PrintVariable(c)

	c <- variable

	time.Sleep(5 * time.Second)
	data := <-c // читаем значение из канала, мы ждем тут, пока в этот канал ничего не положат

	fmt.Println(data)
}
