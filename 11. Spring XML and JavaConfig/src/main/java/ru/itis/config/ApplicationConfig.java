package ru.itis.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import ru.itis.repositories.UsersRepository;
import ru.itis.repositories.UsersRepositoryJdbcTemplateImpl;
import ru.itis.services.UsersService;
import ru.itis.services.impl.UsersServiceImpl;
import ru.itis.ui.UI;
import ru.itis.validation.PasswordValidator;
import ru.itis.validation.impl.PasswordByCharactersValidator;
import ru.itis.validation.impl.PasswordByLengthValidator;

import javax.sql.DataSource;

@PropertySource("classpath:application.properties")
public class ApplicationConfig {

    @Autowired
    private Environment environment;

    @Bean
    public UI ui(UsersService usersService) {
        return new UI(usersService);
    }

    @Bean
    public UsersService usersService(UsersRepository usersRepository,
                                     PasswordValidator passwordValidatorByCharacters) {
        return new UsersServiceImpl(passwordValidatorByCharacters,usersRepository);
    }

    @Bean
    public PasswordValidator passwordValidatorByLength(@Value(("${byLengthValidator.minLength}")) int minLength) {
        return new PasswordByLengthValidator(minLength);
    }

    @Bean
    public PasswordValidator passwordValidatorByCharacters(@Value("${byCharactersValidator.special}") String characters) {
        PasswordByCharactersValidator validator = new  PasswordByCharactersValidator();
        validator.setSpecialCharacters(characters);
        return validator;
    }

    @Bean
    public UsersRepository usersRepository(DataSource dataSource) {
        return new UsersRepositoryJdbcTemplateImpl(dataSource);
    }

    @Bean
    public DataSource dataSource(HikariConfig hikariConfig) {
        return new HikariDataSource(hikariConfig);
    }

    @Bean
    public HikariConfig hikariConfig() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setMaximumPoolSize(environment.getProperty("db.hikari.maxPoolSize", Integer.TYPE));
        hikariConfig.setUsername(environment.getProperty("db.username"));
        hikariConfig.setPassword(environment.getProperty("db.password"));
        hikariConfig.setDriverClassName(environment.getProperty("db.driverClassName"));
        hikariConfig.setJdbcUrl(environment.getProperty("db.url"));

        return hikariConfig;
    }
}
