package example02;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) throws ReflectiveOperationException {
        Class<?> aClass = Class.forName("example01.PasswordValidator");

        Constructor<?> constructor = aClass.getConstructor(String.class, int.class);

        Object object = constructor.newInstance("Bad Password!!!!", 4);

        Method method = aClass.getMethod("validate", String.class, boolean.class);

        Field field = aClass.getDeclaredField("minLength");
        field.setAccessible(true);
        field.set(object, 10);

        Object result = method.invoke(object, "qwerty007", true);

        System.out.println(result);

    }
}
