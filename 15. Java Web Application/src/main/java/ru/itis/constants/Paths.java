package ru.itis.constants;

public class Paths {
    public static final String APPLICATION_PREFIX = "/app";

    public static final String SIGN_UP_PATH = "/signUp";
    public static final String SIGN_IN_PATH = "/signIn";
    public static final String USERS_PATH = "/users";

    public static final String FILES_UPLOAD_PATH = "/files/upload";

    public static final String FILES_PATH = "/files";

    public static final String IMAGES_PATH = "/images";

    public static final String COLOR_CHANGE_PATH = "/profile/color";

    public static final String SEARCH_PAGE = "/users/search.html";

    public static final String PROFILE_PAGE = "/profile.html";

    public static final String SIGN_IN_PAGE = "/signIn.html";

    public static final String USERS_SEARCH_PATH = "/users/search";
}
