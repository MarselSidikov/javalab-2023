package ru.itis.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.dto.UserDto;
import ru.itis.repositories.UsersRepository;
import ru.itis.services.SearchService;

import java.util.Collections;
import java.util.List;

import static ru.itis.dto.UserDto.from;

@RequiredArgsConstructor
@Service
public class SearchServiceImpl implements SearchService {

    private final UsersRepository usersRepository;

    @Override
    public List<UserDto> searchUsers(String query) {
        if (query == null || query.equals("")) {
            return Collections.emptyList();
        }
        return from(usersRepository.findAllByFirstNameOrLastNameLike(query));
    }
}
