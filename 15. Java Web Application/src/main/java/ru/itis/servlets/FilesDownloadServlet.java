package ru.itis.servlets;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import ru.itis.dto.FileDto;
import ru.itis.services.FilesService;

import java.io.IOException;
import java.nio.file.Files;

import static ru.itis.constants.Paths.FILES_PATH;
import static ru.itis.constants.Paths.FILES_UPLOAD_PATH;

//files?fileName=....jar
@WebServlet(FILES_PATH)
@MultipartConfig
public class FilesDownloadServlet extends HttpServlet {

    private FilesService filesService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.filesService = context.getBean(FilesService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String storageFileName = request.getParameter("fileName");
        FileDto file = filesService.getFile(storageFileName);

        response.setContentType(file.getMimeType());
        response.setContentLength(file.getSize().intValue());
        response.setHeader("Content-Disposition", "filename=\"" + file.getOriginalFileName() + "\"");
        Files.copy(file.getPath(), response.getOutputStream());
        response.flushBuffer();
    }
}
