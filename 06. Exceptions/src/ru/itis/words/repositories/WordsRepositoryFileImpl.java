package ru.itis.words.repositories;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WordsRepositoryFileImpl implements WordsRepository {

    private static final String FILE_PREFIX = "content_";

    private final String fileName;

    public WordsRepositoryFileImpl(String fileName) {
        this.fileName = fileName;

        if (!fileName.startsWith(FILE_PREFIX)) {
            throw new IllegalArgumentException("File must have the prefix - " + FILE_PREFIX);
        }
    }

    @Override
    public List<String> findAll() {

        List<String> words = new ArrayList<>();
        try {
            FileReader fileReader = new FileReader(fileName);

            BufferedReader reader = new BufferedReader(fileReader);

            String word = reader.readLine();

            while (word != null) {
                words.add(word);
                word = reader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        return words;
    }
}
