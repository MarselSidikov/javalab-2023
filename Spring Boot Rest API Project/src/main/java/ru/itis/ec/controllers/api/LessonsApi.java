package ru.itis.ec.controllers.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.ec.dto.ExceptionDto;
import ru.itis.ec.dto.LessonDto;
import ru.itis.ec.dto.LessonsPage;
import ru.itis.ec.dto.NewOrUpdateLessonDto;

@Tags(value = {
        @Tag(name = "Lessons")
})
@RequestMapping("/lessons")
public interface LessonsApi {

    @Operation(summary = "Получение списка уроков")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Страница с уроками",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = LessonsPage.class))
                    })
    })
    @GetMapping
    ResponseEntity<LessonsPage> getAllLessons(@Parameter(description = "Номер страницы") @RequestParam("page") int page);

    @Operation(summary = "Добавление урока")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Добавленный урок",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = LessonDto.class))
                    }
            )
    })
    @PostMapping
    ResponseEntity<LessonDto> addLesson(@RequestBody NewOrUpdateLessonDto newLesson);

    @Operation(summary = "Получение урока")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Информация об уроке",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = LessonDto.class))
                    }
            ),
            @ApiResponse(responseCode = "404", description = "Сведения об ошибке",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    }
            )
    })
    @GetMapping("/{lesson-id}")
    ResponseEntity<LessonDto> getLesson(@Parameter(description = "Идентификатор урока", example = "1")
                                        @PathVariable("lesson-id") Long lessonId);

    @Operation(summary = "Обновление урока")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Обновленный урок",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = LessonDto.class))
                    }
            ),
            @ApiResponse(responseCode = "404", description = "Сведения об ошибке",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    }
            )
    })
    @PutMapping("/{lesson-id}")
    ResponseEntity<LessonDto> updateLesson(
            @Parameter(description = "Идентификатор урока", example = "1") @PathVariable("lesson-id") Long lessonId,
            @RequestBody NewOrUpdateLessonDto updatedLesson);

    @Operation(summary = "Удаление урока")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Урок удален"),
            @ApiResponse(responseCode = "404", description = "Сведения об ошибке",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    }
            )
    })
    @DeleteMapping("/{lesson-id}")
    ResponseEntity<?> deleteLesson(
            @Parameter(description = "Идентификатор урока", example = "1") @PathVariable("lesson-id") Long lessonId);

    @Operation(summary = "Публикация урока")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202", description = "Опубликованный урок",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = LessonDto.class))
                    }
            ),
            @ApiResponse(responseCode = "404", description = "Сведения об ошибке",
                    content = {
                            @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ExceptionDto.class))
                    }
            )
    })
    @PutMapping("/{lesson-id}/publish")
    ResponseEntity<LessonDto> publishLesson(
            @Parameter(description = "Идентификатор урока", example = "1") @PathVariable("lesson-id") Long lessonId);
}
