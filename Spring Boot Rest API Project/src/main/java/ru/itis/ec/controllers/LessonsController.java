package ru.itis.ec.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.ec.controllers.api.LessonsApi;
import ru.itis.ec.dto.LessonDto;
import ru.itis.ec.dto.LessonsPage;
import ru.itis.ec.dto.NewOrUpdateLessonDto;
import ru.itis.ec.services.LessonsService;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController

public class LessonsController implements LessonsApi {
    private final LessonsService lessonsService;

    @Override
    public ResponseEntity<LessonsPage> getAllLessons(int page) {
       return ResponseEntity
               .ok(lessonsService.getAllLessons(page));
    }

   @Override
    public ResponseEntity<LessonDto> addLesson(@Valid NewOrUpdateLessonDto newLesson) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(lessonsService.addLesson(newLesson));
    }

    @Override
    public ResponseEntity<LessonDto> getLesson(Long lessonId) {
        return ResponseEntity.ok(lessonsService.getLesson(lessonId));
    }

    @Override
    public ResponseEntity<LessonDto> updateLesson(Long lessonId, @Valid NewOrUpdateLessonDto updatedLesson) {
        return ResponseEntity.accepted().body(lessonsService.updateLesson(lessonId, updatedLesson));
    }

    @Override
    public ResponseEntity<?> deleteLesson(Long lessonId) {
        lessonsService.deleteLesson(lessonId);
        return ResponseEntity.accepted().build();
    }

    @Override
    public ResponseEntity<LessonDto> publishLesson(Long lessonId) {
        return ResponseEntity.accepted().body(lessonsService.publishLesson(lessonId));
    }

}
