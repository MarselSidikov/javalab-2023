package ru.itis.ec.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Страница с уроками и общее количество страниц")
public class LessonsPage {

    @Schema(description = "список уроков")
    private List<LessonDto> lessons;

    @Schema(description = "общее количество страниц", example = "5")
    private Integer totalPagesCount;
}
