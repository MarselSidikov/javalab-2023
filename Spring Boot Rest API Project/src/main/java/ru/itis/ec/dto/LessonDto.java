package ru.itis.ec.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.ec.models.Lesson;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Урок")
public class LessonDto {

    @Schema(description = "идентификатор урока", example = "1")
    private Long id;
    @Schema(description = "название урока", example = "Java")
    private String name;
    @Schema(description = "описание  урока", example = "Очень интересный урок")
    private String description;

    public static LessonDto from(Lesson lesson) {
        return LessonDto.builder()
                .id(lesson.getId())
                .name(lesson.getName())
                .description(lesson.getDescription())
                .build();
    }

    public static List<LessonDto> from(List<Lesson> lessons) {
        return lessons
                .stream()
                .map(LessonDto::from)
                .collect(Collectors.toList());
    }
}
