package ru.itis.ec.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.ec.validation.constraints.NotSameValues;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(description = "Новый урок")
@NotSameValues(fields = {"name", "description"}, message = "{newLesson.values.same}")
public class NewOrUpdateLessonDto {

    @Schema(description = "название урока", example = "Java")
    @Size(min = 2, max = 15, message = "{newLesson.name.size}")
    private String name;

    @Schema(description = "описание  урока", example = "Очень интересный урок")
    @NotEmpty(message = "{newLesson.description.empty}")
    private String description;
}
