package ru.itis.ec.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.itis.ec.dto.LessonDto;
import ru.itis.ec.dto.LessonsPage;
import ru.itis.ec.dto.NewOrUpdateLessonDto;
import ru.itis.ec.exceptions.NotFoundException;
import ru.itis.ec.models.Lesson;
import ru.itis.ec.repositories.LessonsRepository;
import ru.itis.ec.services.LessonsService;

import static ru.itis.ec.dto.LessonDto.from;

@RequiredArgsConstructor
@Service
public class LessonsServiceImpl implements LessonsService {

    private final LessonsRepository lessonsRepository;

    @Value("${default.page-size}")
    private int defaultPageSize;

    @Override
    public LessonsPage getAllLessons(int page) {
        PageRequest pageRequest = PageRequest.of(page, defaultPageSize);
        Page<Lesson> lessonsPage = lessonsRepository.findAllByStateOrderById(pageRequest, Lesson.State.ACTIVE);

        return LessonsPage.builder()
                .lessons(from(lessonsPage.getContent()))
                .totalPagesCount(lessonsPage.getTotalPages())
                .build();
    }

    @Override
    public LessonDto addLesson(NewOrUpdateLessonDto newLesson) {
        Lesson lesson = Lesson.builder()
                .name(newLesson.getName())
                .state(Lesson.State.DRAFT)
                .description(newLesson.getDescription())
                .build();

        lessonsRepository.save(lesson);

        return from(lesson);
    }

    @Override
    public LessonDto getLesson(Long lessonId) {
        Lesson lesson = getLessonOrThrow(lessonId);
        return from(lesson);
    }

    @Override
    public LessonDto updateLesson(Long lessonId, NewOrUpdateLessonDto updatedLesson) {
        Lesson lessonForUpdate = getLessonOrThrow(lessonId);

        lessonForUpdate.setName(updatedLesson.getName());
        lessonForUpdate.setDescription(updatedLesson.getDescription());

        lessonsRepository.save(lessonForUpdate);

        return from(lessonForUpdate);
    }

    @Override
    public void deleteLesson(Long lessonId) {
        Lesson lessonForDelete = getLessonOrThrow(lessonId);

        lessonForDelete.setState(Lesson.State.DELETED);
        lessonsRepository.save(lessonForDelete);
    }

    @Override
    public LessonDto publishLesson(Long lessonId) {
        Lesson lessonForPublish = getLessonOrThrow(lessonId);

        lessonForPublish.setState(Lesson.State.ACTIVE);
        lessonsRepository.save(lessonForPublish);

        return from(lessonForPublish);
    }

    private Lesson getLessonOrThrow(Long lessonId) {
        return lessonsRepository.findById(lessonId)
                .orElseThrow(() -> new NotFoundException("Урок с идентификатором <" + lessonId + "> не найден"));
    }
}
