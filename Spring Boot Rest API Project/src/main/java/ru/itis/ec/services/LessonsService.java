package ru.itis.ec.services;

import ru.itis.ec.dto.LessonDto;
import ru.itis.ec.dto.LessonsPage;
import ru.itis.ec.dto.NewOrUpdateLessonDto;

public interface LessonsService {
    LessonsPage getAllLessons(int page);

    LessonDto addLesson(NewOrUpdateLessonDto newLesson);

    LessonDto getLesson(Long lessonId);

    LessonDto updateLesson(Long lessonId, NewOrUpdateLessonDto updatedLesson);

    void deleteLesson(Long lessonId);

    LessonDto publishLesson(Long lessonId);
}
