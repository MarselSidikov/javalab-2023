package ru.itis.validation.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.itis.exceptions.PasswordValidationException;
import ru.itis.validation.PasswordValidator;

@Component
public class PasswordByCharactersValidator implements PasswordValidator {
    @Value("${byCharactersValidator.special}")
    private String specialCharacters;

    public void validate(String password) throws PasswordValidationException {
        int count = 0;

        for (char character : specialCharacters.toCharArray()) {
            if (password.indexOf(character) != -1) {
                count++;
            }
        }

        if (count == 0) {
            throw new PasswordValidationException("Missing special characters");
        }
    }
}
