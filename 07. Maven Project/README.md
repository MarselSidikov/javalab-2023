# Инструмент Maven

* Сборка проектов - получение собранного исполняемого/библиотечного архива (`.jar`, `.war`) из файлов исходного кода и ресурсов

* Работа с зависимостями - подключение сторонних библиотек в ваш проект

* У Maven-проекта есть набор важных характеристик, называемых координатами

- `GroupId` - идентификатор компании-разработчика, `ru.itis`, `com.oracle`
- `ArtifactId` - идентификатор конкретного проекта
- `Version` - версия конкретного проекта

## Общие принципы работы

* `maven archetype` - структура проекта, которая может быть сгенерирована Maven. Есть встроенные, а есть самописные (в IDEA).

### Базовый Archetype

* `Project`
    * `src` - исходный код, ресурсы
        * `main` - основные файлы исходного кода и ресурсы
            * `java`
            * `resources`
        * `test` - файлы исходного кода модульных и интеграционных тестов, а также тестовые ресурсы
            * `java`
            * `resources`
    * `target` - скомпилированные файлы, а также собранные архивы
    * `pom.xml` - файл проекта (параметры проекта и зависимости)

## Жизненный цикл (Lifecycle) Maven

* По умолчанию есть три жизненных цикла - `default`, `clean`, `site`
* Жизненный цикл состоит из набора `phases`

# Phase

* Элемент жизненного цикла сборки, у каждой фазы есть назначение и в рамках жизненного цикла они всегда исполняются друг за другом.
* Фазы привязываются к плагинам и их конкретным `goal` (в рамках одной фазы может быть несколько  `goals`)

- Lifecycle `clean`
  -  phase `clean`
- Lifecycle `default`
  - `validate` - validate the project is correct and all necessary information is available
  - `compile` - compile the source code of the project
  - `test` - test the compiled source code using a suitable unit testing framework. These tests should not require the code be packaged or deployed
  - `package` - take the compiled code and package it in its distributable format, such as a JAR.
  - `verify` - run any checks on results of integration tests to ensure quality criteria are met
  - `install` - install the package into the local repository, for use as a dependency in other projects locally
  - `deploy` - done in the build environment, copies the final package to the remote repository for sharing with other developers and projects.


## Plugins

* Инструмент, который имеет набор `goals`, которые могут быть привязаны к `phases`

Плагин можно вызвать отдельно:

```
mvn org.apache.maven.plugins:maven-compiler-plugin:3.1:compile
```

## Общая схема выполнения

- Lifecycle
  - phase 1
    - plugin:goal-1
    - plugin:goal-2
  - phase 2
    - plugin:goal-3
    - plugin:goal-4


###  Как происходит работа с зависимостями?

- сначала Maven сканирует папку `.m2`
- если там ничего нет, идем в Central Maven Repository (реестр всех доступных библиотек)
- скачивает ее оттуда в `.m2` и подключает к проекту

### Что нужно изучим дальше:

- многомодульные проекты
- написание плагинов
- dependency-management
- дополнительно скину видео про ручную сборку jar
