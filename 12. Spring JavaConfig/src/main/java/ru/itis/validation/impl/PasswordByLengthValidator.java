package ru.itis.validation.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.itis.exceptions.PasswordValidationException;
import ru.itis.validation.PasswordValidator;

@Component
public class PasswordByLengthValidator implements PasswordValidator {

    private final int minLength;

    public PasswordByLengthValidator(@Value(("${byLengthValidator.minLength}")) int minLength) {
        this.minLength = minLength;
    }

    public void validate(String password) throws PasswordValidationException {
        if (password.length() < minLength) {
            throw new PasswordValidationException("Short password");
        }
    }
}
