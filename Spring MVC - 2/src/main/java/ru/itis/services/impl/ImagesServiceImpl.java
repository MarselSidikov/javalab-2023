package ru.itis.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.repositories.FilesRepository;
import ru.itis.services.ImagesService;

import java.util.List;

@RequiredArgsConstructor
@Service
public class ImagesServiceImpl implements ImagesService {

    private final FilesRepository filesRepository;

    @Override
    public List<String> findAllStorageNamesOfImages() {
        return filesRepository.findAllStorageNamesByType("image/jpeg", "image/png");
    }
}
