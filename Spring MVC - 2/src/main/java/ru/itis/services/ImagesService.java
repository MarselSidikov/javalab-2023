package ru.itis.services;

import java.util.List;

public interface ImagesService {
    List<String> findAllStorageNamesOfImages();
}
