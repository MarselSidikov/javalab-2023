package ru.itis.services;

import ru.itis.dto.SignUpDto;
import ru.itis.dto.UserDto;
import ru.itis.models.User;

import java.util.List;

public interface UsersService {
    List<UserDto> signUp(SignUpDto signUpData);
    List<UserDto> getAllUsers();

    List<User> getAllUsersByAge(int ageFrom, int ageTo);

    List<UserDto> searchUsers(String query);
}
