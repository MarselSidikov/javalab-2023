package ru.itis.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Driver {

    private Long id;

    private String firstName;
    private String lastName;
    private Integer experience;

    private List<Car> cars;
}
