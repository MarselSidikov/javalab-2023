package ru.itis.models;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(exclude = "owner")
@EqualsAndHashCode(exclude = "owner")
public class Car {

    private Long id;

    private String color;
    private String model;
    private Integer mileage;

    private Driver owner;
}
