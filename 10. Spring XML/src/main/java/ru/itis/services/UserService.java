package ru.itis.services;

public interface UserService {
    boolean signUp(String email, String password);
}
