package ru.itis.validation;

import ru.itis.exceptions.PasswordValidationException;

public interface PasswordValidator {
    void validate(String password) throws PasswordValidationException;
}
