# 1. Clean Code

## Правила хорошего кода

### Оформление кода

1. Именование переменных - camelCase

```
int c; // плохо
int steps_count; // не дай бог
int count; // хорошо
int stepsCount; // отлично
```

Можно именовать однобуквенно в циклах.

2. Именование методов - camelCase, важен смысл метода. 

Метод - всегда глагол (если он не boolean)

```
public int func() { ... } // плохо
public boolean isActive() { ... } // хорошо
public double getAverage() { ... } // хорошо для операции, которая выполняется за O(1)
public double calcAverage() { ... } // отлично
```

3. Именование классов (+ интерфейсов) - PascalCase, всегда - существительное, абстрактные классы - `AbstractBin`

```
ISerializable -> Serializable

```

4. Скобки и вложенные скобки

```
class SomeClass {
    public int someFunction(int arg, int anotherArg) {
        whilec (arg < anotherArg) {
            if (...) {
                int hello = "hello";
            }
            arg++;
        }
        
        return arg;
    }
}
```

5. Константы

```
public static final int MAX_DIZE = 10;
```

### Компоненты кода

1. Методы

- делают только то, что указано в их названии.

```
public static void sort(int[] array) {
    for ( ... ) {
        ...
        for ( ... ) {
            ...
        }
    }
    
    // System.out.println(Arrays.toString(array)); - плохо
}

public static void main(String args[]) {
    ...
    sort(array);
    System.out.println(Arrays.toString(array));
    
}
```

2. Классы

* Классы не должны иметь большей ответственности, чем указано в их названии.

```
public class UsersService {
    public List<User> getAll() {
        ...
    }
    
    public void add(User user) {
        ...
    }
    
    // плохо - выделить в отдельный класс
    public void printStackTrace() {
        ...
    }
}
```