package ru.itis.ts.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.ts.models.Task;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import static ru.itis.ts.util.DateTimeUtil.EUROPEAN_DATE_TIME_FORMATTER;

/**
 * 9/9/2023
 * TasksService
 *
 * @author Marsel Sidikov (AIT TR)
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Schema(name = "Task", description = "Задача")
public class TaskDto {

    @Schema(description = "идентификатор задачи", example = "1")
    private Long id;

    @Schema(description = "название задачи", example = "Посадка деревьев", maxLength = 255)
    private String title;

    @Schema(description = "описание задачи", example = "Необходимо посадить деревья в саду", maxLength = 1000)
    private String description;

    @Schema(description = "Дата начала", example = "02.02.2022")
    private String start;

    @Schema(description = "Дата конца", example = "02.03.2022")
    private String finish;

    public static TaskDto from(Task task) {
        TaskDto result = TaskDto.builder()
                .id(task.getId())
                .title(task.getTitle())
                .description(task.getDescription())
                .build();

        if (task.getStart() != null) {
            result.setStart(task.getStart().format(EUROPEAN_DATE_TIME_FORMATTER));
        }

        if (task.getFinish() != null) {
            result.setFinish(task.getFinish().format(EUROPEAN_DATE_TIME_FORMATTER));
        }

        return result;
    }

    public static List<TaskDto> from(List<Task> tasks) {
        return tasks.stream()
                .map(TaskDto::from)
                .collect(Collectors.toList());
    }

}
