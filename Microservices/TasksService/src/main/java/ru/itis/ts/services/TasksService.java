package ru.itis.ts.services;

import ru.itis.ts.dto.NewTaskDto;
import ru.itis.ts.dto.TaskDto;
import ru.itis.ts.dto.TasksPage;

/**
 * 9/9/2023
 * TasksService
 *
 * @author Marsel Sidikov (AIT TR)
 */
public interface TasksService {
    TasksPage getTasks(Integer page);

    TaskDto getTask(Long taskId);

    TaskDto addTask(NewTaskDto newTask);
}
