package ru.itis.ts.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import jakarta.validation.constraints.NotNull;
/**
 * 9/17/2023
 * TasksService
 *
 * @author Marsel Sidikov (AIT TR)
 */
@Data
@Schema(name = "New Task", description = "Задача для добавления")
public class NewTaskDto {

    @Schema(description = "название задачи", example = "Посадка деревьев", maxLength = 255)
    @NotEmpty
    @NotBlank
    @NotNull
    private String title;

    @Schema(description = "описание задачи", example = "Необходимо посадить деревья в саду", maxLength = 1000, minLength = 5)
    @NotEmpty
    @NotBlank
    @NotNull
    @Length(min = 5, max = 1000)
    private String description;

    @Schema(description = "Дата начала", example = "02.02.2022")
    @NotBlank
    @NotEmpty
    private String start;

    @Schema(description = "Дата конца", example = "02.03.2022")
    @NotBlank
    @NotEmpty
    private String finish;
}
