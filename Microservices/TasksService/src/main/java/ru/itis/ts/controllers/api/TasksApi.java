package ru.itis.ts.controllers.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.ts.dto.NewTaskDto;
import ru.itis.ts.dto.StandardResponseDto;
import ru.itis.ts.dto.TaskDto;
import ru.itis.ts.dto.TasksPage;
import ru.itis.ts.validation.dto.ValidationErrorsDto;


/**
 * 9/9/2023
 * TasksService
 *
 * @author Marsel Sidikov (AIT TR)
 */
@Tags(value =
@Tag(name = "Tasks"))
@RequestMapping("/api/tasks")
public interface TasksApi {

    @Operation(summary = "Получение списка задач", description = "Доступно всем пользователям")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Запрос обработан успешно",
                    content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = TasksPage.class))
                    })
    })
    @GetMapping
    ResponseEntity<TasksPage> getTasks(@Parameter(description = "Номер страницы", example = "1") @RequestParam("page") Integer page);

    @Operation(summary = "Получение информации о задаче", description = "Доступно всем пользователям")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Запрос обработан успешно",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = TaskDto.class))
                    }),
            @ApiResponse(responseCode = "404", description = "Задача не найдена",
            content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = StandardResponseDto.class))
            })
    })
    @GetMapping("/{task-id}")
    ResponseEntity<TaskDto> getTask(@Parameter(description = "Идентификатор задачи", example = "1")
                                    @PathVariable("task-id") Long taskId);

    @Operation(summary = "Добавление задачи", description = "Доступно только менеджерам")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Задача добавлена успешно",
                    content = {
                            @Content(mediaType = "application/json", schema = @Schema(implementation = TaskDto.class))
                    }),
            @ApiResponse(responseCode = "400", description = "Ошибка валидации",
            content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = ValidationErrorsDto.class))
            })
    })
    @PostMapping
    ResponseEntity<TaskDto> addTask(@RequestBody @Valid NewTaskDto newTask);
}
